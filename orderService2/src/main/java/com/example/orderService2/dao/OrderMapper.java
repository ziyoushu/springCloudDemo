package com.example.orderService2.dao;

import com.example.orderService2.po.Stock;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface OrderMapper {

    @Select("select id,stock from stock where id = 1")
    @ResultType(Stock.class)
    Stock getStock(Integer id);

    @Update("update stock set stock=stock-1 where id =1")
    Integer dueStock(Integer id);
}
