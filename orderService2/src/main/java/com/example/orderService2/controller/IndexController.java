package com.example.orderService2.controller;

import com.example.orderService2.dao.OrderMapper;
import com.example.orderService2.po.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {

    @Autowired
    OrderMapper orderMapper;

    @RequestMapping("/createOrder")
    @ResponseBody
    public String createOrder() {
        synchronized (this) {
            Stock stock = orderMapper.getStock(1);
            if(stock.getStock() == 0) {
                return "no stock";
            }
            System.out.println("剩余stock:"+stock.getStock());
            orderMapper.dueStock(1);
        }
        return "success";
    }
}
