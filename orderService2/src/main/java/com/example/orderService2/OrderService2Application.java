package com.example.orderService2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class OrderService2Application {

    public static void main(String[] args) {
        SpringApplication.run(OrderService2Application.class, args);
    }

}
