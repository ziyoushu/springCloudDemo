package com.example.userService1.api;

import com.example.userService1.dao.UserMapper;
import com.example.userService1.po.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {

    @Autowired
    UserMapper userMapper;
    @RequestMapping("/getUserInfo")
    @ResponseBody
    public String getUserInfo(Integer uId) {
        User userInfo = userMapper.getUserInfo(1);
        return userInfo.getuName();
    }
}
