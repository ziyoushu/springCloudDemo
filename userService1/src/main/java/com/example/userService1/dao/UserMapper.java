package com.example.userService1.dao;

import com.example.userService1.po.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {

    @Select("select u_id uId, u_name uName from zys_user where u_id = #{uId}")
    User getUserInfo(@Param("uId") Integer uId);
}
