package com.example.orderService1.controller;

import com.example.orderService1.api.UserApi;
import com.example.orderService1.dao.OrderMapper;
import com.example.orderService1.po.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {

    @Autowired
    OrderMapper orderMapper;
    @Autowired
    UserApi userApi;

    @RequestMapping("/createOrder")
    @ResponseBody
    public String createOrder() {
        synchronized (this) {
            Stock stock = orderMapper.getStock(1);
            if(stock.getStock() == 0) {
                return "no stock";
            }
            System.out.println("剩余stock:"+stock.getStock());
            String userInfo = userApi.getUserInfo(1);
            System.out.println(userInfo);
            orderMapper.dueStock(1);
        }

        return "success";
    }
}
