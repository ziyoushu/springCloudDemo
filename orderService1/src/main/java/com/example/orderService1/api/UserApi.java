package com.example.orderService1.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("userService")
public interface UserApi {

    @RequestMapping(value = "/getUserInfo", method = RequestMethod.GET)
    public String getUserInfo(Integer uId);
}
