package com.example.orderService1.utils;


import com.alibaba.fastjson.JSON;
import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.alias.Alias;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.*;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EsUtils {

    public static void main(String[] args) throws IOException {
        String esHost = "125.71.203.101:21023";
        String[] split = esHost.split(",");
        List<HttpHost> hostList = new ArrayList<>();
        for (String s : split) {
            String[] split1 = s.split(":");
            HttpHost httpHost = new HttpHost(split1[0], Integer.valueOf(split1[1]));
            hostList.add(httpHost);
        }
//
        HttpHost[] httpHosts = hostList.toArray(new HttpHost[]{});
        RestClientBuilder builder = RestClient.builder(httpHosts);
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(builder);


        Map<String, Object> settingMap = new HashMap<>();
        settingMap.put("number_of_shards", "3");
        settingMap.put("number_of_replicas", "2");

        Map<String, Object> mappingMap = new HashMap<>();
        mappingMap.put("name", "keyword");
        mappingMap.put("year", "text");
        CreateIndexRequest createIndexRequest = new CreateIndexRequest("order").settings(settingMap);
        createIndexRequest.alias(new Alias("od"));

        RequestOptions options = RequestOptions.DEFAULT;
        IndicesClient indices = restHighLevelClient.indices();

        CreateIndexResponse createIndexResponse = indices.create(createIndexRequest, options);
//
        for (Integer i = 0; i < 500; i++) {
            IndexRequest indexRequest = new IndexRequest("order", "mobile", i + "");
            Map<String, Object> appMap = new HashMap<>();
            appMap.put("name", "apple" + i);
            appMap.put("year", "2019");
            String source = JSON.toJSONString(appMap);
            indexRequest.source(source, XContentType.JSON);
            IndexResponse index = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        }
//        GetRequest getRequest = new GetRequest("order", "mobile", 1+"");
//        GetResponse documentFields = restHighLevelClient.get(getRequest, RequestOptions.DEFAULT);
//        Map<String, Object> sourceAsMap = documentFields.getSourceAsMap();
//        System.out.println(sourceAsMap);


    }
}
