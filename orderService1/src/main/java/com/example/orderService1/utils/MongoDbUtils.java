package com.example.orderService1.utils;


import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MongoDbUtils {

    public static void main(String[] args) throws IOException {

        MongoClientURI uri = new MongoClientURI("mongodb+srv://lee:Bing1314@zys.jrqxi.mongodb.net/zys?retryWrites=true&w=majority");
        MongoClient mongoClient = new MongoClient(uri);
        MongoDatabase xx = mongoClient.getDatabase("zys");
        MongoCollection<Document> ccc = xx.getCollection("article");

//        List<Document> documents = new ArrayList<>();

//        for(Integer i = 1000;i<2000;i++) {
//            Document document = new Document();
//            document.put("age"+i, "xxx");
//            documents.add(document);
//        }
//        System.out.println("xxxxxxxxxxx");
//        long l = System.currentTimeMillis();
//        ccc.insertMany(documents);
//        long l2 = System.currentTimeMillis();
//
//        System.out.println(l2-l);
//        FindIterable<Document> documents = ccc.find();
//        MongoCursor<Document> iterator = documents.iterator();
//        while(iterator.hasNext()) {
//            Document next = iterator.next();
//            Set<String> strings = next.keySet();
//            for (String string : strings) {
//                System.out.println(string+":"+next.get(string));
//            }
//        }


        Document document = new Document("$group", "age1997");
        FindIterable<Document> documents = ccc.find(document);
        MongoCursor<Document> iterator = documents.iterator();
        while(iterator.hasNext()) {
            Document next = iterator.next();
            Set<String> strings = next.keySet();
            for (String string : strings) {
                System.out.println(string+":"+next.get(string));
            }
        }

    }
}
