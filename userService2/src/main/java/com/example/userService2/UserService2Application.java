package com.example.userService2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@SpringBootApplication
@EnableDiscoveryClient
@Controller
@EnableFeignClients
public class UserService2Application {

    @Value("${aa}")
    private String name;
    @Value("${server.port}")
    private String port;

    public static void main(String[] args) {
        SpringApplication.run(UserService2Application.class, args);
    }


    @RequestMapping("/test")
    @ResponseBody
    public String getString() {
        String a ="服务器2"+ name+"：端口"+port;
        return a;
    }
}
